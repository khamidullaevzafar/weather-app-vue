import axios from "axios"
import { API_KEY, BASE_URL, UNITS } from "../config"

export default {
  state: {
    weatherInfo: {},
  },

  mutations: {
    setWeatherInfo(state, payload) {
      state.weatherInfo = payload
    },
  },

  getters: {
    getWeatherInfo(state) {
      return state.weatherInfo
    },
    weatherType(state) {
      if (!state.weatherInfo.weather) return null

      return state.weatherInfo.weather[0].main.toLowerCase()
    },
  },

  actions: {
    async updateWeatherInfo({ commit, getters }) {
      commit("showFullScreenLoader")
      try {
        const response = await axios({
          method: "GET",
          baseURL: BASE_URL,
          url: "/weather",
          params: {
            appid: API_KEY,
            id: getters.getCurrentCity.id,
            units: UNITS
          },
        })
        commit("setWeatherInfo", response.data)
      } catch (error) {
        console.log(error)
      } finally {
        commit("hideFullScreenLoader")
      }
    },
  },
}
