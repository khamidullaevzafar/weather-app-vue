import Vue from "vue"
import Vuex from "vuex"
import appModule from "./appModule"
import citiesModule from "./citiesModule"
import weatherModule from "./weatherModule"

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    appModule,
    citiesModule,
    weatherModule,
  },
})
