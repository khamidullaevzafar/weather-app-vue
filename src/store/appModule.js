export default {
  state: {
    fullScreenLoader: false,
    listLoader: false,
  },
  mutations: {
    showFullScreenLoader(state) {
      state.fullScreenLoader = true
    },
    hideFullScreenLoader(state) {
      state.fullScreenLoader = false
    },
    showListLoader(state) {
      state.listLoader = true
    },
    hideListLoader(state) {
      state.listLoader = false
    },
  },
  getters: {
    getFullScreenLoader(state) {
      return state.fullScreenLoader
    },
    getListLoader(state) {
      return state.listLoader
    },
  },
  actions: {},
}
