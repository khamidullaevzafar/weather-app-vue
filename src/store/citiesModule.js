import axios from "axios"
import { API_KEY, BASE_URL } from "../config"
import citiesOfUzbekistan from "../utils/citiesOfUzbekistan.json"

export default {
  state: {
    citiesList: citiesOfUzbekistan,
    currentCity:
      JSON.parse(localStorage.getItem("currentCity")) ?? citiesOfUzbekistan[0],
  },

  mutations: {
    setCitiesList(state, payload) {
      state.citiesList = payload
    },
    resetCitiesList(state) {
      state.citiesList = citiesOfUzbekistan
    },
    setCurrentCity(state, payload) {
      localStorage.setItem('currentCity', JSON.stringify(payload))
      state.currentCity = payload
    },
  },

  getters: {
    getCitiesList(state) {
      return state.citiesList
    },
    getCurrentCity(state) {
      return state.currentCity
    },
  },
  actions: {
    async searchCities({ commit }, payload) {
      commit("showListLoader")
      try {
        const response = await axios({
          method: "GET",
          baseURL: BASE_URL,
          url: "/find",
          params: {
            appid: API_KEY,
            q: payload,
          },
        })
        commit("setCitiesList", response.data.list)
      } catch (error) {
        console.log(error)
      } finally {
        commit("hideListLoader")
      }
    },
  },
}
